"use strict";

const axios = require("axios");
const config = require("./config.json");
const seedLabels = require("./seed/labels.json");

(async () => {
  axios.defaults.baseURL = config.baseURL;
  axios.defaults.headers.common["PRIVATE-TOKEN"] = config.token;

  async function seedGroupLabel(label) {
    try {
      const res = await axios.post("/groups/gitlab-org/labels", {
        name: label.title,
        color: label.color,
      });

      return res.data;
    } catch (e) {
      console.error(
        `Label seed error =>`,
        JSON.stringify({ name: label.title })
      );
      console.error(e);
    }
  }

  for (const label of seedLabels) {
    const data = await seedGroupLabel(label);
    console.log(
      `Label seeded =>`,
      JSON.stringify({ id: data.id, name: data.name })
    );
  }
})();
